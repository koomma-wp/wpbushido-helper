<?php
/**
 * WPBushido Plugin
 *
 * @package WPBushido
 */

namespace WPBushido\Admin;

use Timber\Timber;

class AdminPage
{
    public static function registerHooks()
    {

    }

    public static function displayAdmin($incomingContext, $timberContext = false, $plugin_path = false, $cache = false)
    {
        $timber = new Timber();
        if (!$timberContext) {
            $context = $timber::get_context();
        }
        else {
            $context = array();
        }
        $context = array_merge($context, $incomingContext);
        if (!$plugin_path) {
            $plugin_path = plugin_dir_path( __FILE__ );
        }
        $pathViews = $plugin_path.'../../ressources/views';
        if (isset($context['twig_tpl_cache']) && $context['twig_tpl_cache'] == true) {
            $timber::$cache = true;
        }
        $timber::$locations = $pathViews;
        $timber::render($context['use_tpl'], $context);
    }

    public static function compileAdmin($incomingContext, $timberContext = false, $plugin_path = false, $cache = false)
    {
        $timber = new Timber();
        if (!$timberContext) {
            $context = $timber::get_context();
        }
        else {
            $context = array();
        }
        $context = array_merge($context, $incomingContext);
        if (!$plugin_path) {
            $plugin_path = plugin_dir_path( __FILE__ );
        }
        $pathViews = $plugin_path.'../../ressources/views';
        if (isset($context['twig_tpl_cache']) && $context['twig_tpl_cache'] == true) {
            $timber::$cache = true;
        }
        $timber::$locations = $pathViews;
        return $timber::compile($context['use_tpl'], $context);
    }
}