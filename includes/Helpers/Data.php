<?php
/**
 * WPBushido Plugin
 *
 * @package WPBushido
 */

namespace WPBushido\Helpers;

use DateTime;
use Exception;

class Data
{
    /**
     * Sanitize a value with the sanitizer passed as argument
     * @param  mixed $value Value to sanitize
     * @param  string $sanitizer Type of sanitizer to apply
     * @return mixed            Sanitized value
     */
    public static function sanitize($value, $sanitizer = 'string', $extraOptions = [])
    {
        switch ($sanitizer) {
            case 'string':
                $value = sanitize_text_field($value);
                break;

            case 'html':
                $value = wp_kses_post($value);
                break;

            case 'slug':
                $value = sanitize_title($value);
                break;

            case 'id':
                $value = sanitize_key($value);
                break;

            case 'email':
                $value = sanitize_email($value);
                break;

            case 'int':
                $value = absint($value);
                break;

            case 'intnull':
                $value = absint($value);
                if (empty($value)) {
                    $value = null;
                }
                break;

            case 'float':
                $value = floatval($value);
                break;

            case 'url':
                $value = esc_url($value, array('http', 'https'));
                break;

            case 'bool':
                $value = (is_string($value) ? ($value == 'true' || $value == '1' ? true : false) : ($value ? true : false));
                break;

            case 'bool_null':
                if ($value !== null) $value = (is_string($value) ? ($value == 'true' || $value == '1' ? true : false) : ($value ? true : false));
                break;

            case 'bool_string':
                $value = (is_string($value) ? ($value == 'true' || $value == '1' ? 'true' : 'false') : ($value ? 'true' : 'false'));
                break;

            case 'textarea':
                $value = explode("\r\n", $value);
                foreach ($value as $val) {
                    $values[] = sanitize_text_field($val);
                }
                $value = implode("<br>", $values);
                break;

            case 'logo':
                $value = wp_check_filetype($value);
                if (in_array($value['ext'], array('jpg', 'jpeg', 'png'))) {
                    $value = true;
                } else {
                    $value = false;
                }
                break;

            case 'datetime':
                if ($value instanceof DateTime === false) {
                    $result = false;
                    try {
                        $result = new DateTime($value);
                    } catch (Exception $e) {
                    }
                    $value = $result;
                }
                break;

            case 'phone':
                if (class_exists('\libphonenumber\PhoneNumberUtil')) {
                    $country = isset($extraOptions['country']) ? $extraOptions['country'] : 'FR';
                    try {
                        $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
                        $number = $phoneUtil->parse($value, $country);
                        if ($phoneUtil->isValidNumber($number)) {
                            $number = $phoneUtil->formatInOriginalFormat($number, $country);
                            $value = implode('.', explode(' ', $number));
                        }
                    } catch (Exception $e) {
                        // Exception
                    }
                }
                break;

            case false:
                break;
        }
        return $value;
    }

    /**
     * Remove non printable utf8 characters from given string
     *
     * @param string $text
     * @return string
     */
    public static function cleanNonPrintableUtf8($text)
    {
        return is_string($text) ? preg_replace('/[\x00-\x08\x0B\x0C\x0E-\x1F\x7F-\x9F]/u', '', $text) : $text;
    }

    /**
     * Get the reading time of the given text
     *
     * @param string $text
     * @param integer $speed Reading speed in word by minute
     * @return array
     */
    public static function getReadingTime($text, $speed = 200)
    {
        $word = str_word_count(strip_tags($text));
        $raw_m = $word / $speed;
        $m = floor($raw_m);
        $s = floor($word % $speed / ($speed / 60));
        $est = $m . ' minute' . ($m == 1 ? '' : 's') . ', ' . $s . ' second' . ($s == 1 ? '' : 's');

        return [
            'minutes' => $m,
            'seconds' => $s,
            'total_seconds' => ($m * 60) + $s,
            'rounded_minutes' => ceil($raw_m),
        ];
    }

    /**
     * Check equal conditional value
     *
     * @param mixed $valueToTest
     * @param mixed $value
     * @return boolean (true or false)
     */
    public static function conditionEqual($valueToTest, $value)
    {
        return $valueToTest == $value;
    }

    /**
     * Check inferior conditional value
     *
     * @param int $valueToTest
     * @param int $value
     * @return boolean (true or false)
     */
    public static function conditionInferior($valueToTest, $value)
    {
        return $valueToTest < $value;
    }

    /**
     * Check superior conditional value
     *
     * @param int $valueToTest
     * @param int $value
     * @return boolean (true or false)
     */
    public static function conditionSuperior($valueToTest, $value)
    {
        return $valueToTest > $value;
    }

    /**
     * Parse and clean incoming REQUEST (post and get) parameters
     *
     * @return array $return - cleaned request data
     */
    public static function checkIncomingAjax()
    {
        $escapeParams = array(
            'action',
            'render'
        );
        $request = $_REQUEST;
        $return = array();
        foreach ($request as $reqK => $reqV) {
            if (!in_array($reqK, $escapeParams)) {
                $return[$reqK] = self::cleanIncomingArg($reqV);
            }
        }
        return $return;
    }

    /**
     * Check superior conditional value
     *
     * @param int $valueToTest
     * @param int $value
     * @return boolean (true or false)
     */
    public static function cleanIncomingArg($arg)
    {
        $argToClean = $arg;
        if (!is_array($argToClean)) {
            if (!filter_var($argToClean, FILTER_VALIDATE_EMAIL)) {
                if (!strstr($argToClean, '/api/')) {
                    $argToClean = urldecode($argToClean);
                }
            }
            if (!strstr($argToClean, '/api/')) {
                $argToClean = sanitize_text_field($argToClean);
            }
            $arg = wp_strip_all_tags($argToClean);
        }
        else {
            $argToClean = array_map('sanitize_text_field', $argToClean);
            $arg = array_map('wp_strip_all_tags', $argToClean);
        }
        return $arg;
    }

    /**
     * Get translation
     *
     * @param string $key
     * @param string $domain
     * @return string
     */
    public static function getTranslation($key, $domain = null)
    {
        if (empty($domain)) {
            if (define('WPTM_LANGUAGE_DOMAIN')) {
                $domain = WPTM_LANGUAGE_DOMAIN;
            }
        }
        return __($key, $domain);
    }


    /**
     * Replace dynamic stopword #mystopword# by real value
     *
     * @param string $string
     * @param mixed $context
     * @param mixed $verbatimMode
     * @return string
     */
    public static function replaceDynamicData($string, $context, $verbatimMode = false)
    {
        $findStopWordsExpression = array();
        preg_match_all('(\#[^#;]+\#)', $string, $matches);
        if (isset($matches[0])) {
            $matches = $matches[0];
        }
        foreach ($matches as $match) {
            $cleanStopWords = $match;
            $cleanStopClean = '';
            preg_match('(\(.*\))', $match, $matchesInto);
            if (count($matchesInto) > 0) {
                foreach ($matchesInto as $matchesIntoItem) {
                    $cleanStopWords = str_replace(array($matchesIntoItem, '#'), array('',''), $match);
                    $cleanStopClean = str_replace(array('(',')'), array('', ''), $matchesIntoItem);
                }
            } else {
                $cleanStopWords = str_replace(array('#'), array(''), $match);
            }
            $findStopWordsExpression[] = array('stopword' => $cleanStopWords, 'expression' => $cleanStopClean, 'original' => $match);
        }
        foreach ($findStopWordsExpression as $stopWordK) {
            $stopWord = $stopWordK['stopword'];
            $expressionWord = $stopWordK['expression'];
            $originalWords = $stopWordK['original'];
            $replaceValue = '';
            $value = self::findInContextValue($stopWord, $context);

            // Case Date
            $isDate = '';
            if ($value instanceof \DateTime || ($verbatimMode && self::isDateStopWord($stopWord))) {
                if ($expressionWord) {
                    if (!$verbatimMode) {
                        $replaceValue = $value->format($expressionWord);
                    }
                    $isDate = '|date("'.$expressionWord.'")';
                } else {
                    if (!$verbatimMode) {
                        $replaceValue = $value->format('d/m/Y');
                    }
                    $isDate = '|date("d/m/Y")';
                }
            } else {
                if (!empty($value)) {
                    if (!empty($expressionWord)) {
                        $replaceValue = str_replace('%s', $value, $expressionWord);
                    } else {
                        $replaceValue = $value;
                    }
                }
            }
            if (!strstr($cleanStopWords, 'style=') && !strstr($cleanStopWords, 'class=')) {
                if ($verbatimMode) {
                    $string = str_replace($originalWords, "{{ " . $stopWord . $isDate . " }}", $string);
                } else {
                    $string = str_replace($originalWords, $replaceValue, $string);
                }
            }
        }
        $string = trim($string);
        $string = str_replace('  ', ' ', $string);
        return $string;
    }


    /**
     * Get context object / array value from a sting stopword (with .)
     *
     * @param string $stopWord
     * @param mixed $context
     * @return string
     */
    public static function findInContextValue($stopWord, $context)
    {
        $value = '';
        $stopWordObject = explode('.', $stopWord);
        $findObject = $context;
        $deepCounter = 0;
        foreach ($stopWordObject as $deep) {
            $deepCounter++;
            if (is_array($findObject) && isset($findObject[$deep])) {
                $findObject = $findObject[$deep];
                if (count($stopWordObject) == $deepCounter) {
                    $value = $findObject;
                }
            } else if (is_object($findObject)) {
                $getMethod = 'get'.ucfirst($deep);
                $findObject = $findObject->$getMethod();
                if (count($stopWordObject) == $deepCounter) {
                    $value = $findObject;
                }
            }
        }
        if ($time = strtotime($value)) {
            $date = new \DateTime();
            $date->setTimestamp($time);
            $date->setTimezone(new \DateTimeZone('Europe/Paris'));
            $value = $date;
        }
        return $value;
    }

    public static function proximitySearch($origin_lat, $origin_lng, $address_lat, $address_lng, $unit)
    {
        $lat1 = $origin_lat;
        $lng1 = $origin_lng;

        // get lat and lng from the address field on the custom post type
        $lat2 = $address_lat;
        $lng2 = $address_lng;

        // calculate distance between locations
        $theta=$lng1-$lng2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        // adjust calculation depending on unit
        if ($unit == "K"){
            return ($miles * 1.609344);
        }
        else if ($unit =="N"){
            return ($miles * 0.8684);
        }
        else{
            return $miles;
        }
    }

    public static function isDateStopWord($stopWord)
    {
        $isDate = false;
        $dateTypes = array('createdAt', 'updatedAt', 'sendAt');
        foreach ($dateTypes as $dateType) {
            if (strstr($stopWord, $dateType)) {
                $isDate = true;
            }
        }
        if (!$isDate && strstr($stopWord, 'Date')) {
            $isDate = true;
        }
        return $isDate;
    }
}
