<?php
/**
 *
 * @author Philippe AUTIER <philippe.autier@koomma.fr>
 */

namespace WPBushido\Api\ApiClient;

use WPBushido\Api\Services\TokenService;
use WPBushido\Api\Services\TokenSessionService;
use WPBushido\Api\Entity\Token;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\Psr7\Uri;
use GuzzleHttp\Psr7\Request as GuzzleRequest;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use GuzzleHttp\Exception\TransferException;
use Cache\Adapter\Redis;

class ApiClient
{
    const TOKEN_METHOD_GET = 'GET';
    const TOKEN_METHOD_POST = 'POST';

    const TOKEN_GET_AUTHMODE_FORM = 'form_params';
    const TOKEN_GET_AUTHMODE_AUTH = 'auth';
    const TOKEN_GET_AUTHMODE_JSON = 'json';
    const TOKEN_GET_AUTHMODE_OAUTH = 'oauth';

    const TOKEN_AUTH_QUERY   = 'query';
    const TOKEN_AUTH_HEADERS = 'headers';
    const TOKEN_AUTH_BEARER  = 'bearer';

    const TOKEN_CACHE_TRANSIENT = 'transient';
    const TOKEN_CACHE_SESSION   = 'session';

    /**
     * @var boolean
     */
    protected $hasAuth = false;

    /**
     * @var string
     */
    protected $typeClient;

    /**
     * @var string
     */
    protected $login;

    /**
     * @var string
     */
    protected $password;

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var Token
     */
    protected $token;

    /**
     * @var string
     */
    protected $tokenName;

    /**
     * @var string
     */
    protected $tokenPath;

    /**
     * @var string
     */
    protected $tokenMethod;

    /**
     * JSON Node of response body (null if directly the response body)
     *
     * @var mixed
     */
    protected $tokenBody;

    /**
     * Type of auth (form_params, auth)
     *
     * @var string
     */
    protected $tokenGetAuthMode;

    /**
     * Methods Auth to use ("query", "headers", etc.)
     *
     * @var string
     */
    protected $tokenAuthMode;

    /**
     * Field to use with methods auth ("token", "Authorization", "Bearer", etc.)
     *
     * @var [type]
     */
    protected $tokenAuthField;

    /**
     * Method used for caching token (transient, session)
     *
     * @var string
     */
    protected $tokenCache = self::TOKEN_CACHE_TRANSIENT;

    /**
     * @var integer
     */
    protected $maxRetries = 1;

    /**
     * @var integer
     */
    protected $currentRetries = 0;

    /**
     * @var array
     */
    protected $cacheOptions;

    /**
     * @var array
     */
    protected $defaultOptions     = array(
        'connect_timeout' => 5.0,
        'timeout'         => 30.0,
        'defaults'        => array(
            'headers' => array(
                'Accept' => 'application/json',
            )
        ),
    );

    /**
     * @var array
     */
    protected $options = [];

    /**
     * @var boolean
     */
    protected $isRetrying = false;

    /**
     * @var boolean
     */
    protected $debug = false;

    /**
     * @param array $clientOptions
     * @param array $guzzleOptions
     */
    public function __construct(array $clientOptions = [], array $guzzleOptions = [])
    {
        if ($this->isDefined('tokenname', $clientOptions)) {
            $this->tokenName = $clientOptions['tokenname'];
        }
        if ($this->isDefined('tokenmethod', $clientOptions)) {
            $this->tokenMethod = $clientOptions['tokenmethod'];
        }
        if ($this->isDefined('tokenbody', $clientOptions)) {
            $this->tokenBody = $clientOptions['tokenbody'];
        }
        if ($this->isDefined('tokengetauthmode', $clientOptions)) {
            $this->tokenGetAuthMode = $clientOptions['tokengetauthmode'];
        }
        if ($this->isDefined('tokenauthmode', $clientOptions)) {
            $this->tokenAuthMode = $clientOptions['tokenauthmode'];
        }
        if ($this->isDefined('tokenauthfield', $clientOptions)) {
            $this->tokenAuthField = $clientOptions['tokenauthfield'];
        }
        if ($this->isDefined('tokenpath', $clientOptions)) {
            $this->tokenPath = $clientOptions['tokenpath'];
        }
        if ($this->isDefined('tokencache', $clientOptions) && \in_array(strtolower($clientOptions['tokencache']), [self::TOKEN_CACHE_TRANSIENT, self::TOKEN_CACHE_SESSION])) {
            $this->tokenCache = strtolower($clientOptions['tokencache']);
        }
        if ($this->isDefined('type', $clientOptions)) {
            $this->typeClient = $clientOptions['type'];
        }
        if ($this->isDefined('token', $clientOptions)) {
            $this->setToken($clientOptions['token']);
        }
        if ($this->isDefined('username', $clientOptions)) {
            $this->login = $clientOptions['username'];
        }
        if ($this->isDefined('password', $clientOptions)) {
            $this->password = $clientOptions['password'];
        }
        if ($this->isDefined('cache', $clientOptions)) {
            $this->cacheOptions = $clientOptions['cache'];
        }
        if ($this->isDefined('maxretries', $clientOptions)) {
            $this->maxRetries = $clientOptions['maxretries'];
        }
        if ($this->isDefined('debug', $clientOptions)) {
            $this->debug = true;
            $this->options = array_merge_recursive($this->defaultOptions, array('debug' => $this->debug));
        }

        if (!empty($this->tokenName)
            && !empty($this->tokenMethod)
            && !empty($this->tokenGetAuthMode)
            && !empty($this->tokenAuthMode)
            && !empty($this->tokenPath)) {
            $this->hasAuth = true;
        }

        /*----------  Options  ----------*/
        $this->options = array_merge_recursive($this->options, $guzzleOptions, array(
            'handler' => $this->createHandlerStack()
        ));

        /*----------  Initialize Client  ----------*/
        /**
         * @var Client
         */
        $this->client = new Client($this->options);
    }

    /**
     * Make API Request
     *
     * @param string $endpoint
     * @param array $options
     * @return mixed
     */
    public function request($method = 'GET', $endpoint, array $options = [], $withheader = array(), $cache = false)
    {
        /*  Current time & expire */
        $timeCurrent = new \DateTime();
        $data = array();
        $cachedData = false;
        if ($cache && self::checkCache()) {
            $cachedData = self::requestGetCache($endpoint, $options, $cache);
        }
        if (!$cachedData) {
            try {
                $response = $this->client->request($method, $endpoint, $options);
                $statusCode = $response->getStatusCode();
                if ($statusCode == 200 || $statusCode == 201) {
                    $body = $response->getBody();
                    if (isset($withheader) && count($withheader) > 0) {
                        foreach ($withheader as $header) {
                            $data[$header] = $response->getHeader($header);
                        }
                        $data['data'] = json_decode($body, true);
                    } else {
                        $data = json_decode($body, true);
                    }
                    if ($cache && self::checkCache()) {
                        self::requestSaveCache($endpoint, $options, $cache, $data);
                    }
                    return $data;
                }
            } catch (\Exception $e) {
                $data = $this->handlingException($e);
            }
        }
        else {
            $data = $cachedData;
        }

        return $data;
    }

    public function checkCache()
    {
        $cache = false;
        if (isset($this->cacheOptions['activation']) && $this->cacheOptions['activation']) {
            if (isset($this->cacheOptions['type']) && $this->cacheOptions['type'] == 'redis') {
                if (array_key_exists('mode', $this->cacheOptions) && $this->cacheOptions['mode'] == 'cluster' && class_exists('\RedisCluster')) {
                    $cache = true;
                } else if (class_exists('\Redis')) {
                    $cache = true;
                }
            }
        }
        return $cache;
    }

    public function requestGetCache($endpoint, $options, $cache)
    {
        $cachedData = false;
        if ($cache && is_array($cache)) {
            if (isset($cache['cache_key'])) {
                $cacheKey = $cache['cache_key'];
            }
            else {
                /* Cache Hash */
                $cacheHash = self::getCacheKey($cache, $endpoint, $options);
                if (isset($this->cacheOptions['type']) && $this->cacheOptions['type'] == 'redis') {
                    if (array_key_exists('mode', $this->cacheOptions) && $this->cacheOptions['mode'] == 'cluster') {
                        $client = new \RedisCluster(null, [$this->cacheOptions['host'] .":". $this->cacheOptions['port']]);
                    }
                    else {
                        $client = new \Redis();
                        $client->connect($this->cacheOptions['host'], $this->cacheOptions['port']);
                    }
                    $pool = new \Cache\Adapter\Redis\RedisCachePool($client);
                    $cacheObj = $pool->getItem($cacheHash);
                    $findCache = $pool->hasItem($cacheHash);
                    if ($findCache && $cacheObj->isHit()) {
                        // Force Clear Cache
                        if (isset($cache['force_clear']) && $cache['force_clear'] == true) {
                            $pool->deleteItem($cacheHash);
                        } else {
                            $cachedData = $this->unCompressCache($cacheObj->get());
                        }
                    }
                }
            }
        }
        return $cachedData;
    }

    public function requestSaveCache($endpoint, $options, $cache, $data)
    {
        $cachedData = false;
        if ($cache && is_array($cache)) {
            if (isset($cache['cache_key'])) {
                $cacheKey = $cache['cache_key'];
            }
            else {
                /* Cache Hash */
                $cacheHash = self::getCacheKey($cache, $endpoint, $options);
                if (isset($this->cacheOptions['type']) && $this->cacheOptions['type'] == 'redis') {
                    if (array_key_exists('mode', $this->cacheOptions) && $this->cacheOptions['mode'] == 'cluster') {
                        $client = new \RedisCluster(null, [$this->cacheOptions['host'] .":". $this->cacheOptions['port']]);
                    }
                    else {
                        $client = new \Redis();
                        $client->connect($this->cacheOptions['host'], $this->cacheOptions['port']);
                    }
                    $pool = new \Cache\Adapter\Redis\RedisCachePool($client);
                    $cacheObj = $pool->getItem($cacheHash);
                    $findCache = $pool->hasItem($cacheHash);
                    $createCache = true;
                    if ($findCache) {
                        if ($cacheObj->isHit()) {
                            $createCache = false;
                        }
                    }
                    if ($createCache && isset($cache['ttl']) && !empty($cache['ttl'])) {
                        $cacheObj->set($this->compressCache($data));
                        $cacheObj->expiresAfter($cache['ttl']);
                        $pool->save($cacheObj);
                    }
                }
            }
        }
        return $cachedData;
    }

    function getCacheKey($cache, $endpoint, $options)
    {
        $prefix = 'cacheapiclient';
        if (isset($cache['prefix']) && !empty($cache['prefix'])) {
            $prefix = $cache['prefix'];
        }
        return $prefix.'_'.sha1($endpoint . json_encode($options));
    }
    /**
     * Get Token
     *
     * @return Token
     */
    private function getToken()
    {
        if ($this->token === null) {
            if ($this->tokenCache === self::TOKEN_CACHE_SESSION) {
                if (($this->token = TokenSessionService::getCachedToken($this->tokenName)) === false) {
                    $this->refreshToken();
                }
            } else {
                if (($this->token = TokenService::getCachedToken($this->tokenName)) === false) {
                    $this->refreshToken();
                }
            }
        }

        if (false === $this->token) {
            throw new \Exception('Invalid auth credential');
        }

        return $this->token->getToken();
    }

    /**
     * @return array|mixed|null
     */
    private function refreshToken()
    {
        if (!$this->hasAuth) {
            return;
        }

        $token = null;
        $requestParams = [];

        if (self::TOKEN_GET_AUTHMODE_FORM === $this->tokenGetAuthMode) {
            $requestParams = [
                'form_params' => [
                    'grant_type' => 'password',
                    'login' => $this->login,
                    'password' => $this->password,
                ],
            ];
        } elseif (self::TOKEN_GET_AUTHMODE_AUTH === $this->tokenGetAuthMode) {
            $requestParams = [
                'auth' => [
                    $this->login,
                    $this->password
                ]
            ];
        } elseif (self::TOKEN_GET_AUTHMODE_JSON === $this->tokenGetAuthMode) {
            $requestParams = [
                'json' => [
                    'username' => $this->login,
                    'password' => $this->password
                ]
            ];
        } elseif (self::TOKEN_GET_AUTHMODE_OAUTH === $this->tokenGetAuthMode) {
            $requestParams = [
                'json' => [
                    'type' => $this->typeClient,
                    'id' => $this->login,
                    'token' => $this->password
                ]
            ];
        } else {
            return false;
        }

        $response = $this->request($this->tokenMethod, $this->tokenPath, $requestParams);


        if (is_wp_error($response)) {
            return false;
        }

        if (!empty($this->tokenBody)) {
            if ($this->isDefined($this->tokenBody, $response)) {
                $token = $response[$this->tokenBody];
            }
        } else {
            $token = $response;
        }

        if ($token) {
            $token = $this->setToken($token);
        }

        return $token;
    }

    /**
     * Set the given token and cache it
     *
     * @param string $token
     * @return Token
     */
    public function setToken($token)
    {
        $this->hasAuth = true;

        if ($this->tokenCache === self::TOKEN_CACHE_SESSION) {
            $this->token = TokenSessionService::save($token, $this->tokenName);
        } else {
            $this->token = TokenService::save($token, $this->tokenName);
        }

        return $this->token;
    }

    /**
     * Create Guzzle  handler stack
     * @return HandlerStack Stack instance
     */
    private function createHandlerStack()
    {
        $stack = HandlerStack::create();

        if ($this->hasAuth) {
            /*----------  Token  ----------*/
            $stack->unshift($this->createAuthMiddleware(), 'api_token');
        }

        /*----------  Retry  ----------*/
        $stack->unshift($this->createRetryMiddleware(), 'api_retry');

        /*----------  Debug middleware  ----------*/
        if ($this->debug === true) {
            $stack->push($this->createDebugMiddleware(), 'api_debug');
        }

        return $stack;
    }

    /**
     * Auth Middleware
     * @return Middleware Middleware
     */
    private function createAuthMiddleware()
    {
        return Middleware::mapRequest(function (GuzzleRequest $request) {
            if (strpos($request->getUri()->getPath(), $this->tokenPath) !== false) {
                return $request;
            }

            if (self::TOKEN_AUTH_QUERY === $this->tokenAuthMode && !empty($this->tokenAuthField)) {
                $request = $request->withUri(Uri::withQueryValue(
                    $request->getUri(),
                    $this->tokenAuthField,
                    $this->getToken()
                ));
            } elseif (self::TOKEN_AUTH_HEADERS === $this->tokenAuthMode) {
                $request = $request->withAddedHeader('Authorization', $this->getToken());
            } elseif (self::TOKEN_AUTH_BEARER === $this->tokenAuthMode) {
                $request = $request->withAddedHeader('Authorization', sprintf('Bearer %s', $this->getToken()));
            }

            return $request;
        });
    }

    /**
     * Retry Middleware
     * @return Middleware Middleware
     */
    private function createRetryMiddleware()
    {
        return Middleware::retry(function(
            $retries,
            \GuzzleHttp\Psr7\Request $request,
            \GuzzleHttp\Psr7\Response $response = null,
            \GuzzleHttp\Exception\RequestException $exception = null
        ){

            if ($exception && $response === null) {
                $response = $exception->getResponse();
            }

            if ($this->currentRetries++ >= $this->maxRetries) {
                return false;
            }

            if ($response) {
                $statusCode = $response->getStatusCode();

                if (401 === $statusCode) {
                    $this->token = $this->refreshToken();
                    $this->isRetrying = true;
                    return true;
                }

                // Retry on server errors
                if ($statusCode > 500) {
                    $this->isRetrying = true;
                    return true;
                }
            }

            if ($exception instanceof TransferException) {
                $this->isRetrying = true;
                return true;
            }

            return false;
        });
    }

    /**
     * Debug Middleware
     * @return callable Returns a function that accepts the next handler
     */
    private function createDebugMiddleware()
    {
        return function (callable $handler) {
            return function (\GuzzleHttp\Psr7\Request $request, array $options) use ($handler) {
                return $handler($request, $options)->then(
                    function ($response) use ($request) {
                        var_dump(array(
                                     'uri' => (string) $request->getUri(),
                                     'headers' => $request->getHeaders(),
                                     'body' => (string) $request->getBody(),
                                     'method' => (string) $request->getMethod()
                                 ));
                        return $response;
                    }
                );
            };
        };
    }

    /**
     * Handle Exception to log error
     * @param  Exception $e Exception object
     */
    private function handlingException($e)
    {
        $response = [
            'response' => null,
            'httpCode' => 0,
            'error'    => true,
        ];

        // Could be useful in case of debugging exception
        // print_r($e->getTraceAsString());

        if ($e instanceof TransferException) {

            if ($e->hasResponse()) {
                $response['response'] = $e->getResponse();
                $response['httpCode'] = $e->getCode();
            } else {
                $response['response'] = $e->getMessage();
                $response['httpCode'] = 0;
            }
        } else {
            $response['response'] = $e->getMessage();
            $response['httpCode'] = 0;
        }

        return new \WP_Error('api_'. $this->tokenName, $response['response'], [
            'httpCode'   => $response['httpCode'],
            'stackTrace' => $e->getTraceAsString(),
        ]);
    }

    /**
     * Check if a given key is defied in array and not empty
     *
     * @param mixed $key
     * @param array $haystack
     * @return boolean
     */
    private function isDefined($key, array $haystack)
    {
        return (is_array($haystack) && isset($haystack[$key]) && !empty($haystack[$key]));
    }


    private function compressCache($data)
    {
        return gzcompress(serialize($data));
    }

    private function unCompressCache($data)
    {
        return unserialize(gzuncompress($data));
    }
}
