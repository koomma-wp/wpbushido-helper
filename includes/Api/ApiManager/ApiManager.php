<?php
/**
 *
 * @author Philippe AUTIER <philippe.autier@koomma.fr>
 */

namespace WPBushido\Api\ApiManager;

use WPBushido\Api\ApiClient\ApiClient;

class ApiManager
{

    /**
     * @var ApiClient
     */
    protected $client;

    /**
     * @var string
     */
    protected $baseUri = '/api';

    /**
     * Current API Manager instance
     *
     * @var self
     */
    private static $_instance = null;

    /**
     * Create or retrieve instance of ApiManager
     *
     * @return self
     */
    public static function instance()
    {
        if (self::$_instance == null) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function __construct()
    {
        $this->client = new ApiClient([
        ]);
    }
}