<?php
/**
 *
 * @author Philippe AUTIER <philippe.autier@koomma.fr>
 */

namespace WPBushido\Api\Entity;

/**
 * Class Token
 */
class Token
{
    /**
     * Id
     */
    protected $id;

    /**
     * Token
     */
    protected $token;

    /**
     * Creation
     *
     * @var \DateTime $created
     */
    protected $created;

    /**
     * Name
     */
    protected $name;

    /**
     * Counterpart constructor.
     * @param $token
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param mixed $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
}
