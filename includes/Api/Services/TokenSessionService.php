<?php

/**
 * Service
 *
 * @author Philippe AUTIER <philippe.autier@koomma.fr>
 */

namespace WPBushido\Api\Services;

use WPBushido\Api\Entity\Token;

/**
 * Class TokenSessionService
 *
 * @package WPBushido\Services
 */
class TokenSessionService
{

    /**
     * Token duration in DateInterval string format
     *
     * @var string
     */
    protected static $tokenDuration = 12 * HOUR_IN_SECONDS;

    /**
     * Get cached token
     *
     * @param string $name
     * @return Token|boolean
     */
    public static function getCachedToken($name)
    {
        if (!self::sessionStarted() || !isset($_SESSION[self::getCacheKey($name)])) {
            return false;
        }
        $token = $_SESSION[self::getCacheKey($name)];

        if ($token !== false) {
            $token = unserialize($token);

            if (self::isOutdated($token)) {
                return false;
            }
        }

        return $token;
    }

    /**
     * @param $value
     * @return Token
     */
    public static function save($value, $name, $created = null)
    {
        $token = self::getCachedToken($name);

        if ($token === false) {
            $token = new Token($value);
            $token->setName($name);
        } else {
            $token->setToken($value);
        }

        if (null !== $created) {
            $token->setCreated($created);
        } else {
            $now = new \DateTime();
            $token->setCreated($now);
        }

        $expires = $token->getCreated();
        if (null === $expires) {
            $expires = new \DateTime();
        }
        $expires = $expires->add(new \DateInterval('PT'. self::$tokenDuration .'S'));

        if (self::sessionStarted()) {
            $_SESSION[self::getCacheKey($name)] = serialize($token);
        }

        return $token;
    }

    /**
     * @param Token $token
     */
    public static function remove(Token $token)
    {
        $name = $token->getName();

        if (self::sessionStarted() && isset($_SESSION[self::getCacheKey($name)])) {
            unset($_SESSION[self::getCacheKey($name)]);
            return true;
        }

        return false;
    }

    /**
     * @param Token $token
     * @return bool
     */
    public static function isOutdated(Token $token)
    {
        $tokenDate = $token->getCreated();
        $tokenDate->add(new \DateInterval('PT'. self::$tokenDuration .'S'));

        $now = new \DateTime();

        return ($now > $tokenDate);
    }

    /**
     * Get Cache Key for Token
     *
     * @param string $name
     * @return string
     */
    public static function getCacheKey($name)
    {
        return $name.'_token';
    }

    /**
     * Check if session exists
     *
     * @return boolean
     */
    private static function sessionStarted()
    {
        return !(session_id() == '' || !isset($_SESSION));
    }
}
