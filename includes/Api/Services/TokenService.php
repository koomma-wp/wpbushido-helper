<?php

/**
 * Service
 *
 * @author Philippe AUTIER <philippe.autier@koomma.fr>
 */

namespace WPBushido\Api\Services;

use WPBushido\Api\Entity\Token;

/**
 * Class TokenService
 *
 * @package WPBushido\Services
 */
class TokenService
{

    /**
     * Token duration in DateInterval string format
     *
     * @var string
     */
    protected static $tokenDuration = 12 * HOUR_IN_SECONDS;

    /**
     * Get cached token
     *
     * @param string $name
     * @return Token|boolean
     */
    public static function getCachedToken($name)
    {
        $token = get_transient(self::getCacheKey($name));

        if ($token !== false) {
            $token = unserialize($token);
        }

        return $token;
    }

    /**
     * @param $value
     * @return Token
     */
    public static function save($value, $name, $created = null)
    {
        $token = self::getCachedToken($name);

        if ($token === false) {
            $token = new Token($value);
            $token->setName($name);
        } else {
            $token->setToken($value);
        }

        if (null !== $created) {
            $token->setCreated($created);
        } else {
            $now = new \DateTime();
            $token->setCreated($now);
        }

        $expires = $token->getCreated();
        if (null === $expires) {
            $expires = new \DateTime();
        }
        $expires = $expires->add(new \DateInterval('PT'. self::$tokenDuration .'S'));

        set_transient(self::getCacheKey($name), serialize($token), self::$tokenDuration);

        return $token;
    }

    /**
     * @param Token $token
     */
    public static function remove(Token $token)
    {
        $name = $token->getName();

        return delete_transient(self::getCacheKey($name));
    }

    /**
     * @param Token $token
     * @return bool
     */
    public static function isOutdated(Token $token)
    {
        $tokenDate = $token->getCreated();
        $tokenDate->add(new \DateInterval('PT'. self::$tokenDuration .'S'));

        $now = new \DateTime();

        return ($now > $tokenDate);
    }

    /**
     * Get Cache Key for Token
     *
     * @param string $name
     * @return string
     */
    public static function getCacheKey($name)
    {
        return $name.'_token';
    }
}
