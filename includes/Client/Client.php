<?php
/**
 * WPBushido Plugin
 *
 * @package WPBushido
 */

namespace WPBushido\Client;

class Client extends \Timber\Site
{

    public function __construct() {
        \Timber::$dirname = 'templates';
        /*if (WP_TIMBER_USE_CACHE == true) {
            \Timber::$cache = true;
        }*/
        add_filter('timber_context', array($this, 'addToContext'));
        add_filter('get_twig', array($this, 'addToTwig'));
        add_action('wp_ajax_nopriv_timber_ajax', array($this, 'timberAjax'));
        add_action('wp_ajax_timber_ajax', array($this, 'timberAjax'));
        //add_filter('timber/cache/location', array($this, 'customTimberTwigCache'));
        parent::__construct();
    }

    /*public function customTimberTwigCache() {
        return WP_TIMBER_CACHE_DIR . '/timber';
    }*/

    /**
     * Timber Start Content Hook
     *
     * @param array $context
     * @return void
     */
    public function addToContext($context)
    {
        $pageId = get_queried_object_id();
        // get level of the page
        $context['site'] = $this;

        if (isset($_REQUEST['site_id']) && !empty($_REQUEST['site_id'])) {
            switch_to_blog(intval($_REQUEST['site_id']));
            $context['site'] = new \Timber\Site(intval($_REQUEST['site_id']));
        }

        if ($pageId) {
            $context['page_id'] = $pageId;
            $context['page_object'] = new \Timber\Post($pageId);
            if (class_exists('ACF')) {
                $context['page_acf'] = get_fields($pageId);
            }
            $context['is_home_page'] = is_front_page();
        }
        if (class_exists('ACF')) {
            $context['options'] = get_fields('option');
        }

        return $context;
    }

    /**
     * Timber Twig Function Definition
     *
     * @param object $twig
     * @return object $twig
     */
    public function addToTwig(\Twig_Environment $twig)
    {
        $twig->addFunction(new \Timber\Twig_Function('getTemplateAssetFunction', array($this, 'getTemplateAssetFunction')));
        $twig->addFunction(new \Timber\Twig_Function('returnStylePathFile', array($this, 'returnStylePathFile')));
        $twig->addFunction(new \Timber\Twig_Function('returnParentStylePathFile', array($this, 'returnParentStylePathFile')));
        $twig->addFunction(new \Timber\Twig_Function('getStylePathFile', array($this, 'getStylePathFile')));
        $twig->addFunction(new \Timber\Twig_Function('getParentStylePathFile', array($this, 'getParentStylePathFile')));
        $twig->addFunction(new \Timber\Twig_Function('loadAcfImage', array($this, 'loadAcfImage')));
        $twig->addFunction(new \Timber\Twig_Function('getTranslation', array($this, 'getTranslation')));
        $twig->addFunction(new \Timber\Twig_Function('getTimberProcessUrl', array($this, 'getTimberProcessUrl')));
        $twig->addFunction(new \Timber\Twig_Function('renderInclude', array($this, 'renderInclude')));
        $twig->addFunction(new \Timber\Twig_Function('displayMoreRead', array($this, 'displayMoreRead')));
        $twig->addFunction(new \Timber\Twig_Function('getHash', array($this, 'getHash')));
        return $twig;
    }

    /**
     * Include Controller Ajax Php
     *
     * @return void
     */
    public function timberAjax()
    {
        if (isset($_REQUEST['render'])) {
            $include = sanitize_text_field($_REQUEST['render']);
            $ajaxController = get_stylesheet_directory().'/controller/ajax/'.$include.'.php';
            if (file_exists($ajaxController)) {
                include_once $ajaxController;
            }
        }
        die();
    }

    /**
     * Get content of given asset from theme directory
     *
     * @param string $asset
     * @param string $imgAttributes
     * @param boolean $imgFallback
     * @return string
     */
    public function getTemplateAssetFunction($asset, $imgAttributes = '', $imgFallback = false, $forceId = false)
    {
        try {
            $ctx = stream_context_create(array('http'=>
                array(
                    'timeout' => 15, // In seconds
                )
            ));

            $newForceId = false;
            if (!empty($forceId)) {
                $this->clientSvgInclude++;
                if (\is_array($forceId)) {
                    $clientSvgInclude = $this->clientSvgInclude;
                    $newForceId = array_map(function($val) use ($clientSvgInclude) {
                        return $val.$clientSvgInclude;
                    }, $forceId);
                } else {
                    $newForceId = $forceId.'-'.$this->clientSvgInclude;
                }
            }

            $file = file_get_contents(get_stylesheet_directory() . $asset, false, $ctx);

            $file = str_ireplace('<svg', '<svg '. $imgAttributes, $file);

            if ($newForceId) {
                $file = str_ireplace($forceId , $newForceId, $file);
            }

            return $file;

        } catch (\Exception $e) {
            // File not found ?
        }

        if ($imgFallback) {
            return '<img src='. get_stylesheet_directory_uri() . $asset .'" '. $imgAttributes .' />';
        }

        return '';
    }

    /**
     * Display theme file full url
     *
     * @param string $file
     * @return void
     */
    public function getStylePathFile($file)
    {
        echo $this->returnStylePathFile($file);
    }

    /**
     * Return theme file full url
     *
     * @param string $file
     * @return string
     */
    public function returnStylePathFile($file)
    {
        return get_stylesheet_directory_uri().$file;
    }

    /**
     * Display parent theme file full url
     *
     * @param string $file
     * @return void
     */
    public function getParentStylePathFile($file)
    {
        echo $this->returnParentStylePathFile($file);
    }

    /**
     * Return parent theme file full url
     *
     * @param string $file
     * @return string
     */
    public function returnParentStylePathFile($file)
    {
        return get_template_directory_uri().$file;
    }

    /**
     * Display img tag from Acf image object
     *
     * @param array $fileAcf
     * @param string $class
     * @param string $alt
     * @return void
     */
    public function loadAcfImage($fileAcf, $class = '', $alt='')
    {
        echo \WPBushido\Helpers\Media::getImageFromACFData($fileAcf, false, [
            'class' => $class,
            'alt' => $alt
        ]);
    }

    /**
     * Get translation
     *
     * @param string $key
     * @param string $domain
     * @return string
     */
    public function getTranslation($key, $domain = null)
    {
        return \WPBushido\Helpers\Data::getTranslation($key, $domain);
    }

    /**
     * Render HInclude Tags and Content
     *
     * @param string $url
     * @param array $params
     * @param string $entry
     * @param string $defaultcontent
     * @param string $defaulttwig
     * @return void
     */
    public function renderInclude($url, $params = array(), $entry = '', $defaultcontent = '', $defaulttwig = '')
    {
        $returnTag = '';
        if (!empty($url)) {
            $returnTag.= '<hx:include ';
            if (!empty($entry)) {
                $returnTag.= 'entry="'.$entry.'" ';
            }
            $paramsAdd = '';
            if (count($params) > 0) {
                $paramsAdd = '&'.http_build_query($params);
            }
            $returnTag.= 'src="'.\TimberHelper::ob_function(array($this, 'getTimberProcessUrl'), [$url]).$paramsAdd.'">';
            if (!empty($defaulttwig)) {
                $context = \Timber::get_context();
                $returnTag.= \Timber::compile($defaulttwig, $context);
            }
            else if (!empty($defaultcontent)) {
                $context = \Timber::get_context();
                $returnTag.= $defaultcontent;
            }
            $returnTag.= '</hx:include>';
        }
        echo $returnTag;
    }

    /**
     * Return more read from content
     *
     * @param string $content
     * @return array $newContent
     */
    public function displayMoreRead($content)
    {
        $newContent = [];
        if (!empty($content)) {
            $content = strip_tags($content, '<a>');
            $newContent['before'] = '';
            $newContent['after'] = '';

            $cutContent = wordwrap($content, 400, '<!--custom-->', false);
            $split = explode('<!--custom-->', $cutContent);
            foreach ($split as $ind => $text) {

                if ($ind == 0) {
                    $newContent['before'] = $text;
                }
                else {
                    $newContent['after'] .= $text . ' ';
                }
            }
        }

        return $newContent;
    }

    /**
     * Return hashed string
     *
     * @param string $algo
     * @param string $string
     * @return array $newContent
     */
    public static function getHash($algo = 'sha512', $string)
    {
        return hash($algo, $string);
    }

    /**
     * @param $process
     * @param array $params
     */
    public function getTimberProcessUrl($process, $params = array())
    {
        if (substr($process, 0,1) != '/') {
            echo self::returnTimberProcessUrl($process, $params);
        }
        else {
            echo $process;
        }
    }

    public static function returnTimberProcessUrl($process, $params = array())
    {
        $paramsAdd = '';
        if (!isset($params['site_id'])) {
            $params['site_id'] = get_current_blog_id();
        }
        if (count($params) > 0) {
            $paramsAdd = '&'.http_build_query($params);
        }
        if (substr($process, 0,1) != '/') {
            // Clean Process String
            $process = str_replace('?', '&', $process);
            $pageParams = '';
            $pageId = get_queried_object_id();
            if ($pageId && !empty($pageId)) {
                $pageParams = '&post_id='.$pageId;
            }
            return '/wp-admin/' . ('admin-ajax.php') . '?action=timber_ajax&render=' . $process.$paramsAdd.$pageParams;
        }
        else {
            return $process;
        }
    }

    /**
     * Render Template to check 404 error
     *
     * @param string $template
     * @param array $context
     * @param boolean $type
     * @return array $context
     */
    public static function getTimberAjaxProcessUrl($process)
    {
        if (substr($process, 0,1) != '/') {
            // Clean Process String
            $process = str_replace('?', '&', $process);
            $processURL = '/wp-admin/' . ('admin-ajax.php') . '?action=timber_ajax&render=' . $process;
            if (!strstr($processURL, 'site_id')) {
                $processURL.= '&site_id='.get_current_blog_id();
            }
            return $processURL;
        }
        else {
            return $process;
        }
    }

    /**
     * Render Template to check 404 error
     *
     * @param string $template
     * @param array $context
     * @param boolean $type
     * @return array $context
     */
    public static function getRenderTwig($template, $context, $type = false)
    {
        $context['use_tpl'] = $template;
        //$context = self::renderSeo($template, $context, $type);
        if (isset($context['error_404']) && !empty($context['error_404'])) {
            $context['use_tpl'] = '404.html.twig';
        }
        // 404 ERROR
        if ($context['use_tpl'] == '404.html.twig') {
            header_remove('Cache-Control');
            header_remove('Pragma');
            header_remove('Expires');
            header_remove('Status');
            status_header(404);

        }
        return $context;
    }

    /**
     * Get Post template name
     *
     * @param int $post_id
     * @return string $tpl name of the page template
     */
    public static function getTplName($post_id)
    {
        $tpl = get_page_template_slug($post_id);
        if (!empty($tpl)) {
            $tpl = str_replace(array('controller/', '.php'), array('', ''), $tpl);
        }
        else {
            $tpl = false;
        }
        return $tpl;
    }

    /**
     * Get a page slug with template name
     *
     * @param $name_tpl string Name of the template file
     * @return the slug of the page found
     */
    public static function getPageSlugByTpl($name_tpl = '')
    {
        $slug = false;

        $pages = get_posts(array(
            'post_type' => 'page',
            'post_status' => 'publish',
            'meta_key' => '_wp_page_template',
            'meta_value' => 'controller/'. $name_tpl .'.php',
            'posts_per_page' => 1
        ));

        if (!empty($pages)) {
            $page = reset($pages);
            $slug = $page->post_name;
        }

        return $slug;
    }
}
